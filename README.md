download the project using `git clone https://jafroj@gitlab.com/jafroj/BT.git`

Dependencies: vagrant and virtualbox installed on the host

execute by running `vagrant up`

I had no experience with vagrant, so I really haven't figured out why during the execution of the playbook, it can't find the Dockerfile

as you can see from my previous commits, I tried changing the location of the files several times, it didn't work

once the build has "completed" you can ssh into the guest and build the container with `docker build -t web:0.1 .` then run it `docker run -d -p 80:80 --name web web:0.1`

the guest exposes port 80 to the port 8080 of the host

you can reach the web site with `http://localhost:8080`