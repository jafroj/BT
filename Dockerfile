FROM alpine:latest 

RUN	apk update &&	apk upgrade \
	&& apk add --update  nginx bash
RUN	mkdir -p /run/nginx && mkdir -p /var/www/site

ADD nginx.conf /etc/nginx/nginx.conf

ADD index.html /var/www/site/index.html

EXPOSE 80 

CMD ["nginx", "-g",  "daemon off;"]
